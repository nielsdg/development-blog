---
title: "Better memory consumption on annotated PDFs"
date: 2020-01-04T21:28:58+01:00
tags: ["GNOME", "Evince"]
---

I was recently looking at a PDF document and suddenly noticed Evince (GNOME's
PDF reader) was using _a lot_ of memory for a ~100 page article that mostly
contained text. Like a good FLOSS citizen, I looked upstream if there were any
issues that matched what I had just seen, and that's where I found my first
clue on the problem. Someone [reported] that if you tried to load a document
with a lot of annotations, memory consumption would immediately go up by several
hundreds of Megabytes!

Since I contributed to GNOME already, I figured I might as well take a look if
there was any quick fix. I built Evince using jhbuild and created a [heaptrack]
trace. By analyzing that and looking at the source code, I figured out that
Evince was loading each annotation and creating a window for it. Each window
instance contained a spell checker, and that of course needed to maintain a
complex set of rules and known words.

In the end, I created an [MR that kind of fixes the problem] by removing the
line that loaded all the annotation windows at the very beginning. It's a first
step to improve the memory consumption, and even though you could still end up
with the same memory usage after individually opening up each annotation, you at
least won't suffer from this if you have no plans of doing that. Long term,
probably the spell checker should be shared over the whole document (and maybe
also lazily loaded).

One of the big takeaways I wanted to call out in this post though, is that
somebody originally reported the issue and with just enough detail, so that I
could recognize the same symptoms for my situation, _and_ even make a partial
fix for the problem. So if you ever run into a bug: please take the time to go
to the project's issue tracker and file a bug. It's a small but important step
to help FLOSS projects move forward :-)


[heaptrack]: https://github.com/KDE/heaptrack
[reported]: https://gitlab.gnome.org/GNOME/evince/issues/1010
[MR that kind of fixes the problem]: https://gitlab.gnome.org/GNOME/evince/-/merge_requests/207
