---
title: About me
---

My full name is Niels De Graef, but online I usually go by my username nielsdg.
I live in Ghent, Belgium so my timezone is GMT+1 (GMT+2 in DST).

I've been contributing to [GNOME] for quite a few years now, with my first
contribution ever being for [Geary]. These days, I contribute to several other
modules and also maintain several components, such as [GNOME Contacts] and
[Seahorse] \(Passwords and Keys).

I currently work at [Red Hat] as the product owner of the GPU team, which is
part of the Desktop Group. Before that I worked at [Barco], where I dealt with
multimedia projects using several parts from the GNOME stack, Debian and
[GStreamer]. And before _that_, I was a student computer science who learned to
love (and contribute to) open source :-)


[Barco]: https://www.barco.com/en/
[GNOME Contacts]: https://gitlab.gnome.org/GNOME/gnome-contacts/
[GNOME]: https://www.gnome.org
[Geary]: https://gitlab.gnome.org/GNOME/geary/
[Red Hat]: https://www.redhat.com
[Seahorse]: https://gitlab.gnome.org/GNOME/seahorse/
[GStreamer]: https://gstreamer.freedesktop.org/
